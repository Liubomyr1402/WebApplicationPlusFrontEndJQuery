﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplicationDAL.Entities
{
    public class Book
    {
        public int BookId { get; set; }

        public string BookName { get; set; }

        public string Author { get; set; }

        public int Price { get; set; }
    }
}
