﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplicationDAL.EF;
using WebApplicationDAL.Entities;
using WebApplicationDAL.Interfaces;

namespace WebApplicationDAL.Repositories
{
    public class PurchaseRepository : IRepository<Purchase>
    {
        BookPurchaseDbContext dbContext;

        public PurchaseRepository(BookPurchaseDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(Purchase item)
        {
            dbContext.Purchases.Add(item);
        }

        public void Delete(int id)
        {
            Purchase purchaseToDelete = dbContext.Purchases.Find(id);

            if (purchaseToDelete != null)
            {
                dbContext.Purchases.Remove(purchaseToDelete);
            }
        }

        public Purchase Get(int id)
        {
            return dbContext.Purchases.Find(id);
        }

        public IEnumerable<Purchase> GetAll()
        {
            return dbContext.Purchases;
        }

        public void Update(Purchase item)
        {
            dbContext.Entry(item).State = EntityState.Modified;
        }
    }
}
