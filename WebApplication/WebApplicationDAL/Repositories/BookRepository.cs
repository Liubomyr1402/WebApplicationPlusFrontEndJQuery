﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplicationDAL.EF;
using WebApplicationDAL.Entities;
using WebApplicationDAL.Interfaces;

namespace WebApplicationDAL.Repositories
{
    public class BookRepository : IRepository<Book>
    { 
        BookPurchaseDbContext dbContext;

        public BookRepository(BookPurchaseDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(Book item)
        {
            dbContext.Books.Add(item);
        }
        
        public void Delete(int id)
        {
            Book bookToDelete = dbContext.Books.Find(id);

            if (bookToDelete != null)
            {
                dbContext.Books.Remove(bookToDelete);
            }
        }

        public Book Get(int id)
        {
            return dbContext.Books.Find(id);
        }

        public IEnumerable<Book> GetAll()
        {
            return dbContext.Books;
        }

        public void Update(Book item)
        {
            dbContext.Entry(item).State = EntityState.Modified;

        }

        public void Save()
        {
            dbContext.SaveChanges();
        }

    }
}
