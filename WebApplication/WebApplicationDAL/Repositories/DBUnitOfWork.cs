﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplicationDAL.Entities;
using WebApplicationDAL.Interfaces;
using WebApplicationDAL.EF;

namespace WebApplicationDAL.Repositories
{
    public class DBUnitOfWork : IDBUnitOfWork
    {
        BookPurchaseDbContext dbContext;
        BookRepository bookRepository;
        PurchaseRepository purchaseRepository;

        public DBUnitOfWork(BookPurchaseDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IRepository<Book> Books
        {
            get
            {
                if (bookRepository == null)
                {
                    return bookRepository = new BookRepository(dbContext);
                }
                else
                {
                    return bookRepository;
                }
            }
        }

        public IRepository<Purchase> Purchase
        {
            get
            {
                if (purchaseRepository == null)
                {
                    return purchaseRepository = new PurchaseRepository(dbContext);
                }
                else
                {
                    return purchaseRepository;
                }
            }
        }

        public void Save()
        {
            dbContext.SaveChanges();
        }
    }
}
