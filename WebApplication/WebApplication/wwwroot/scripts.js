﻿GetBooks();
GetPurchases();

function CreateBook(book, authorName, bookPrice) {
    $.ajax({
        url: "api/Books",
        contentType: "application/json",
        method: "POST",
        data: JSON.stringify({
            bookName: book,
            author: authorName,
            price: bookPrice
        }),
        success: function (book) {
            reset();
            $("#tblBooks").append(rowWithBooks(book));
        }
    })
}

function CreatePurchase(personName, personAddress, purchaseBookId) {
    $.ajax({
        url: "api/Purchase",
        contentType: "application/json",
        method: "POST",
        data: JSON.stringify({
            person: personName,
            address: personAddress,
            bookId: purchaseBookId
        }),
        success: function (purchase) {
            reset();
            $("#tblPurchases").append(rowWithBooks(purchase));
        }
    })
}

function EditBook(idBook, book, authorName, bookPrice)
{
    $.ajax({
        url: "api/Books/" + idBook,
        contentType: "application/json",
        method: "PUT",
        data: JSON.stringify({
            bookId: idBook,
            bookName: book,
            author: authorName,
            price: bookPrice
        }),
        success: function (book) {
            reset();
        }
    })
}
//EditPurchase(purchaseId, personName, addressName, selectedBookId);
function EditPurchase(purchaseId, personName, addressName, selectedBookId) {
    $.ajax({
        url: "api/Purchase/" + purchaseId,
        contentType: "application/json",
        method: "PUT",
        data: JSON.stringify({
            purchaseId: purchaseId,
            person: personName,
            address: addressName,
            bookId: selectedBookId
        }),
        success: function (purchase) {
            reset();
        }
    })
}

function GetBook(id) {
    $.ajax({
        url: '/api/Books/' + id,
        type: 'GET',
        contentType: "application/json",
        success: function (book) {
            var form = document.forms["bookForm"];
            form.elements["bookId"].value = book.bookId;
            form.elements["bookName"].value = book.bookName;
            form.elements["authorName"].value = book.author;
            form.elements["bookPrice"].value = book.price;
        }
    });
}

function GetPurchase(id) {
    $.ajax({
        url: '/api/Purchase/' + id,
        type: 'GET',
        contentType: "application/json",
        success: function (purchase) {
            var form = document.forms["purchaseForm"];
            form.elements["purchaseId"].value = purchase.purchaseId;
            form.elements["personName"].value = purchase.person;
            form.elements["addressName"].value = purchase.address;
            form.elements["selectedBookId"].value = purchase.bookId;
        }
    });
}


function GetBooks() {
    $.ajax({
        url: '/api/Books',
        type: 'GET',
        contentType: "application/json",
        success: function (Books) {
            var rows = "";
            $.each(Books, function (index, Book) {
                rows += rowWithBooks(Book);
            })
            $("#tblBooks").append(rows);
        },
        error: function () {
            alert("Error!!!");
        }
    });
};

function GetPurchases() {
    $.ajax({
        url: '/api/Purchase',
        type: 'GET',
        contentType: "application/json",
        success: function (Purchases) {
            var rows = "";
            $.each(Purchases, function (index, Purchase) {
                rows += rowWithPurchases(Purchase);
            })
            $("#tblPurchases").append(rows);
        },
        error: function () {
           
            alert("Error!!!");
        }
    });
};

function DeletePurchase(id) {
    $.ajax({
        url: "api/Purchase/" + id,
        contentType: "application/json",
        method: "DELETE",
        success: function (purchase) {
            $("tr[data-rowid='" + purchase.id + "']").remove();
        }
    })
};

function DeleteBook(id) {
    $.ajax({
        url: "api/Books/" + id,
        contentType: "application/json",
        method: "DELETE",
        success: function (book) {
            $("tr[data-rowid='" + book.id + "']").remove();
        }
    })
};



$(document).ready(function () {

    $("body").on("click", ".removePurchaseLink", function () {
        var id = $(this).data("id");
        DeletePurchase(id);
        location.reload();
    })

    $("body").on("click", ".removeBookLink", function () {
        var id = $(this).data("id");
        DeleteBook(id);
        location.reload();
    })

    $("body").on("click", ".editBookLink", function () {
        var id = $(this).data("id");
        GetBook(id);
    })

    $("body").on("click", ".editPurchaseLink", function () {
        var id = $(this).data("id");
        GetPurchase(id);
    })


    $("#bookForm").submit(function (e) {
        e.preventDefault();
        var bookId = this.elements["bookId"].value;
        var bookName = this.elements["bookName"].value;
        var authorName = this.elements["authorName"].value;
        var price = this.elements["bookPrice"].value;
        if (bookId == 0) {
            CreateBook(bookName, authorName, price);
            this.reset();
            this.elements["bookId"].value = 0;
        }
        else
            EditBook(bookId, bookName, authorName, price);
        location.reload();
    });

    $("#purchaseForm").submit(function (e) {
        e.preventDefault();
        var purchaseId = this.elements["purchaseId"].value;
        var personName = this.elements["personName"].value;
        var addressName = this.elements["addressName"].value;
        var selectedBookId = this.elements["selectedBookId"].value;

        if (purchaseId == 0) {
            CreatePurchase(personName, addressName, selectedBookId);
            this.reset();
            this.elements["selectedBookId"].value = 0;
        }
        else
        {
            EditPurchase(purchaseId, personName, addressName, selectedBookId);
        }
        location.reload();
    });

});


var rowWithBooks = function (book) {
    return "<tr data-rowid='" + book.bookId + "'><td>" + book.bookId + "</td>" +
        "<td>" + book.bookName + "</td> <td>" + book.author + "</td> <td>" + book.price + "</td>" +
        "<td><a class='editBookLink' data-id='" + book.bookId + "'>Update</a> | " +
        "<a class='removeBookLink' data-id='" + book.bookId + "'>Delete</a></td></tr>"
};

var rowWithPurchases = function (purchase) {
    return "<tr data-rowid='" + purchase.purchaseId + "'><td>" + purchase.purchaseId + "</td>" +
        "<td>" + purchase.person + "</td> <td>" + purchase.address + "</td> <td>" + purchase.bookId + "</td>" +
        "<td><a class='editPurchaseLink' data-id='" + purchase.purchaseId + "'>Update</a> | " +
        "<a class='removePurchaseLink' data-id='" + purchase.purchaseId + "'>Delete</a></td></tr>"
};
