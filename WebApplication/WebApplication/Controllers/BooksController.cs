﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApplicationDAL.EF;
using WebApplicationDAL.Entities;
using WebApplicationDAL.Repositories;
using WebApplicationDAL.Interfaces;

namespace WebApplication.Controllers
{
    [Route("api/[controller]")]
    public class BooksController : Controller
    {
        BookPurchaseDbContext dbContext;
        IDBUnitOfWork dbUnitOfWork; 

        public BooksController(BookPurchaseDbContext dbContext)
        {
            this.dbContext = dbContext;
            
            dbUnitOfWork = new DBUnitOfWork(this.dbContext);
            
        }
        
        [HttpGet]
        public IEnumerable<Book> Get()
        {
            return dbUnitOfWork.Books.GetAll();
        }

        [HttpGet("{id}")]
        public Book Get(int id)
        {
            Book newBook = dbUnitOfWork.Books.Get(id);

            return newBook;
        }
        
        [HttpPost]
        public void Post([FromBody]Book newBook)
        {
            if (newBook.BookName == null)
            {
                Response.StatusCode = 204;
            }
            else
            {
                dbUnitOfWork.Books.Create(newBook);
                dbUnitOfWork.Save();
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Book bookToUpdate)
        {
            if (bookToUpdate.BookName == null || bookToUpdate.BookId != id)
            {
                Response.StatusCode = 204;
            }
            else
            {
                dbUnitOfWork.Books.Update(bookToUpdate);
                dbUnitOfWork.Save();
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            if (dbUnitOfWork.Books.Get(id) == null)
            {
                Response.StatusCode = 204;
            }
            else
            {
                dbUnitOfWork.Books.Delete(id);
                dbUnitOfWork.Save();
            }
        }
    }
}
